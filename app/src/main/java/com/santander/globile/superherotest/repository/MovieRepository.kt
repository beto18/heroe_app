package com.santander.globile.superherotest.repository

import com.santander.globile.superherotest.model.movie.Movie
import com.santander.globile.superherotest.model.movie.MovieAndDetail
import kotlinx.coroutines.flow.Flow

interface MovieRepository {

    fun getMoviePopular(): Flow<List<Movie>>

    fun findMovieById(id: Int): Flow<Movie>

    suspend fun insertMovies(movies: List<Movie>)

    suspend fun insertMovie(movie: Movie)

    suspend fun deleteMovie(movie: Movie)

    suspend fun deleteMovies(movies: List<Movie>)

    fun findMovieAndDetailById(id: Int): Flow<MovieAndDetail>


}