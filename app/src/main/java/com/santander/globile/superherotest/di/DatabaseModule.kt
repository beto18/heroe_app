package com.santander.globile.superherotest.di

import android.app.Application
import androidx.room.Room
import com.santander.globile.superherotest.BuildConfig
import com.santander.globile.superherotest.data.room.HeroAppDb
import com.santander.globile.superherotest.utils.DATABASE_NAME
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Named
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object DatabaseModule {

    @Provides
    @Named(DATABASE_NAME)
    @Singleton
    fun provideDatabaseName() = BuildConfig.DATABASE_NAME


    @Provides
    @Singleton
    fun provideHeroeDatabase(
        app: Application,
        @Named(DATABASE_NAME) databaseName: String
    ) = Room.databaseBuilder(app, HeroAppDb::class.java, databaseName)
        .fallbackToDestructiveMigration()
        .build()


}
