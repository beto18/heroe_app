package com.santander.globile.superherotest.domain.hero

import com.santander.globile.superherotest.model.hero.Hero

interface ApiHeroHelper {

    suspend fun getHeroes(): List<Hero>

    suspend fun getHero(id: Int): Hero

}