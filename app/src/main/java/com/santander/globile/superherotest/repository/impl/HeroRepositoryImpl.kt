package com.santander.globile.superherotest.repository.impl

import com.santander.globile.superherotest.data.db.hero.HeroDbRepository
import com.santander.globile.superherotest.domain.hero.ApiHeroHelper
import com.santander.globile.superherotest.error.ResourceNotFoundException
import com.santander.globile.superherotest.model.hero.Hero
import com.santander.globile.superherotest.repository.HeroRepository
import com.santander.globile.superherotest.utils.network.ConnectionProvider
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject

class HeroRepositoryImpl
@Inject constructor(
    private val apiHelper: ApiHeroHelper,
    private val db: HeroDbRepository,
    private val connectionProvider: ConnectionProvider
) : HeroRepository {
    companion object {
        private val TAG = HeroRepositoryImpl::class.java.name
    }

    override fun getHeroes(): Flow<List<Hero>> = flow {
        var heroes: List<Hero>? = null
        connectionProvider.isConnected()?.let { conneceted ->
            if (conneceted) {
                heroes = apiHelper.getHeroes()
                insertHeroes(heroes!!)
            } else {
                heroes = db.findHeroes()
            }

        }


        emit(heroes!!)

    }.flowOn(Dispatchers.IO)

    override fun findHeroById(id: Int): Flow<Hero> = flow {
        var hero: Hero?
        try {
            hero = db.findHeroById(id)
            if (hero == null) {
                connectionProvider.isConnected()?.let { connected ->
                    if (connected) {
                        hero = apiHelper.getHero(id)
                    } else {
                        throw ResourceNotFoundException()
                    }
                }

            } else {
                emit(hero!!)
            }
        } catch (e: Exception) {
            throw e
        }


    }.flowOn(Dispatchers.IO)

    override suspend fun insertHeroes(heroes: List<Hero>) {
        db.insertHeroes(heroes)
    }

    override suspend fun insertHero(hero: Hero) {
        db.insertHero(hero)
    }

    override suspend fun deleteHero(hero: Hero) {
        db.deleteHero(hero)
    }

    override suspend fun deleteHeroes(heroes: List<Hero>) {
        db.deleteHeroes(heroes)
    }


}