package com.santander.globile.superherotest.data.converter

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.santander.globile.superherotest.model.hero.Work

class WorkConverter {
    @TypeConverter
    fun powerstatsToString(work: Work): String = Gson().toJson(work)

    @TypeConverter
    fun stringToPowerstats(string: String): Work =
        Gson().fromJson(string, Work::class.java)
}