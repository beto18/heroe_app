package com.santander.globile.superherotest.data.room

import androidx.sqlite.db.SimpleSQLiteQuery
import com.santander.globile.superherotest.data.dao.BaseDao
import com.santander.globile.superherotest.data.db.DbRepository
import javax.inject.Inject


open class RoomRepository<T> @Inject constructor(
    private val dao: BaseDao<T>,
) : DbRepository<T> {


    override fun findById(query: String, id: Int): T {
        return dao.findById(getSQLiteQuery(query, arrayOf(id)))
    }

    override fun findAll(query: String): List<T> {
        return dao.findAll(getSQLiteQuery(query))
    }

    override fun insert(entitys: List<T>) {
        dao.insertAll(entitys)
    }

    override fun insert(entity: T) {
        dao.insert(entity)
    }

    override fun delete(entity: T) {
        dao.delete(entity)
    }

    override fun delete(entitys: List<T>) {
        dao.delete(entitys)
    }


    override fun update(entity: T) {
        dao.update(entity)
    }

    override fun update(entitys: List<T>) {
        dao.updateAll(entitys)
    }

    private fun getSQLiteQuery(query: String, params: Array<Any> = arrayOf()): SimpleSQLiteQuery {
        return SimpleSQLiteQuery(query, params)
    }


}