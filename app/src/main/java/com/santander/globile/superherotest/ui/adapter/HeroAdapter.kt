package com.santander.globile.superherotest.ui.adapter

import com.santander.globile.superherotest.databinding.ItemHeroBinding
import com.santander.globile.superherotest.model.hero.Hero
import com.santander.globile.superherotest.ui.base.BaseAdapter
import com.santander.globile.superherotest.ui.base.BaseViewHolder
import com.santander.globile.superherotest.ui.viewholder.HeroViewHolder

class HeroAdapter(override val onItemClicked: (data: Hero) -> Unit) :
    BaseAdapter<Hero, ItemHeroBinding>(ItemHeroBinding::inflate) {

    override fun set(data: List<Hero>) {
        submitList(data)
    }

    override fun viewHolder(binding: ItemHeroBinding): BaseViewHolder<Hero> {
        return HeroViewHolder(binding) {
            onItemClicked(getItem(it))
        }
    }


}