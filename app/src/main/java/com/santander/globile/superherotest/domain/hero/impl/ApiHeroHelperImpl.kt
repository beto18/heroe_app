package com.santander.globile.superherotest.domain.hero.impl

import com.santander.globile.superherotest.domain.hero.ApiHero
import com.santander.globile.superherotest.domain.hero.ApiHeroHelper
import com.santander.globile.superherotest.model.hero.Hero
import javax.inject.Inject

class ApiHeroHelperImpl @Inject
constructor(private val apiHero: ApiHero) : ApiHeroHelper {

    override suspend fun getHeroes(): List<Hero> {
        val list: ArrayList<Hero> = ArrayList()
        for (i in 1..10) {
            val result = apiHero.getHeroForId(i)
            list.add(result)
        }
        return list
    }

    override suspend fun getHero(id: Int): Hero {
        return apiHero.getHeroForId(id)
    }
}