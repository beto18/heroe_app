package com.santander.globile.superherotest.ui.detail.hero

import android.util.Log
import androidx.lifecycle.viewModelScope
import com.santander.globile.superherotest.repository.HeroRepository
import com.santander.globile.superherotest.ui.base.BaseViewModel
import com.santander.globile.superherotest.ui.commons.UIState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@ExperimentalCoroutinesApi
@HiltViewModel
class HeroesDetailViewModel @Inject constructor(
    private val heroRepository: HeroRepository,
) : BaseViewModel() {


    private val _hero by lazy { MutableStateFlow<UIState>(UIState.Empty) }
    val hero: StateFlow<UIState> = _hero


    fun getHero(id: Int) {
        viewModelScope.launch {
            heroRepository.findHeroById(id)
                .catch {
                    Log.e(TAG, it.message, it)
                    _hero.value = UIState.Error(it.message!!)
                }.collect {
                    _hero.value = UIState.Success(it)
                }
        }
    }


}