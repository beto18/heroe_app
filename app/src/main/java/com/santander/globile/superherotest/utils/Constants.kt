package com.santander.globile.superherotest.utils


const val BASE_URL_HEROES = "baseUrlHero"
const val BASE_URL_MOVIES = "baseUrlMovies"
const val DATABASE_NAME = "databaseName"
const val URL_IMAGE = "https://image.tmdb.org/t/p/"
const val SIZE_W500 = "w500"
const val IMAGE_SIZE_W185 = "w185"
const val PROFILE_SIZE_W185 = "w185"
const val POFILE_SIZE_W154 = "w154"
const val BACKDROP_SIZE = "w780"
const val ORIGINAL = "original"
