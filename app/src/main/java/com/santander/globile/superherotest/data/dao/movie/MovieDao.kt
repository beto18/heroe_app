package com.santander.globile.superherotest.data.dao.movie

import androidx.room.Dao
import androidx.room.Query
import androidx.room.Transaction
import com.santander.globile.superherotest.data.dao.BaseDao
import com.santander.globile.superherotest.model.movie.Movie
import com.santander.globile.superherotest.model.movie.MovieAndDetail
import com.santander.globile.superherotest.model.movie.MovieDetail

@Dao
interface MovieDao : BaseDao<Movie>

@Dao
interface MovieDetailDao : BaseDao<MovieDetail>

@Dao
interface MovieAndDetailDao {
    @Transaction
    @Query("SELECT * FROM movie WHERE id = :id LIMIT 1")
    fun findById(id: Int): MovieAndDetail?


}

