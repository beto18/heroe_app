package com.santander.globile.superherotest.aspects

import java.lang.reflect.Proxy

@SuppressWarnings("UNCHECKED")
class Aspects {

    companion object {

        fun <T> aspect(type: Class<T>, implementation: T): T {
            val aspect = Proxy.newProxyInstance(
                type.classLoader,
                arrayOf<Class<*>>(type),
                AspectInvocationHandler(type, implementation)
            )
            return aspect as T

        }
    }

}