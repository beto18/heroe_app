package com.santander.globile.superherotest.data.db.movie

import com.santander.globile.superherotest.model.movie.MovieAndDetail

interface MovieAndDetailRepo {

    fun getMovieAndDetail(id: Int): MovieAndDetail
}