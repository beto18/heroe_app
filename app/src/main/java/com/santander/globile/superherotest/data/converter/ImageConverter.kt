package com.santander.globile.superherotest.data.converter

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.santander.globile.superherotest.model.hero.Image

class ImageConverter {

    @TypeConverter
    fun imageToString(image: Image): String = Gson().toJson(image)

    @TypeConverter
    fun stringToImage(string: String): Image =
        Gson().fromJson(string, Image::class.java)
}