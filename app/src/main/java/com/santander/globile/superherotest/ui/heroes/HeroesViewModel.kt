package com.santander.globile.superherotest.ui.heroes

import android.util.Log
import androidx.lifecycle.viewModelScope
import com.santander.globile.superherotest.repository.HeroRepository
import com.santander.globile.superherotest.ui.adapter.HeroAdapter
import com.santander.globile.superherotest.ui.base.BaseViewModel
import com.santander.globile.superherotest.ui.commons.UIState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@ExperimentalCoroutinesApi
@HiltViewModel
class HeroesViewModel @Inject constructor(
    private val interactor: HeroRepository,
) : BaseViewModel() {

    val heroeAdapter by lazy {
        HeroAdapter {
            Log.d(TAG, "El heroe selccionado es: ${it.name}")
        }
    }


    private val _heroesFlow = MutableStateFlow<UIState>(UIState.Empty)
    val heroesFlow: StateFlow<UIState> = _heroesFlow

    fun getHeroes() {
        viewModelScope.launch {
            interactor.getHeroes()
                .catch {
                    _heroesFlow.emit(UIState.Error(it.message!!))
                }.collect {
                    _heroesFlow.emit(UIState.Success(it))
                }
        }
    }
}