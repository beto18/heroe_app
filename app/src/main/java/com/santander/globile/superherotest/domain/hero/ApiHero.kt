package com.santander.globile.superherotest.domain.hero

import com.santander.globile.superherotest.model.hero.Hero
import retrofit2.http.GET
import retrofit2.http.Path

interface ApiHero {

    @GET("{id}")
    suspend fun getHeroForId(@Path("id") id: Int): Hero

}