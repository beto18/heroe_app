package com.santander.globile.superherotest.data.db


interface DbRepository<T> {

    fun findAll(query: String): List<T>

    fun findById(query: String, id: Int): T?

    fun insert(entitys: List<T>)

    fun insert(entity: T)

    fun delete(entity: T)

    fun delete(entitys: List<T>)

    fun update(entity: T)

    fun update(entitys: List<T>)

}



