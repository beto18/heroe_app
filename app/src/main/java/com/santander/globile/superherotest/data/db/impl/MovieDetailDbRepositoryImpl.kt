package com.santander.globile.superherotest.data.db.impl

import com.santander.globile.superherotest.data.db.movie.MovieDetailDbRepository
import com.santander.globile.superherotest.data.room.HeroAppDb
import com.santander.globile.superherotest.data.room.RoomRepository
import com.santander.globile.superherotest.model.movie.MovieDetail
import javax.inject.Inject

class MovieDetailDbRepositoryImpl @Inject constructor(db: HeroAppDb) :
    RoomRepository<MovieDetail>(db.movieDetailDao),
    MovieDetailDbRepository {


    companion object {

        private const val QUERY_MOVIE_DETAIL = "SELECT * FROM heroe"

        private const val QUERY_MOVIE_BY_ID = "SELECT * FROM heroe WHERE id = ?"
    }

    override suspend fun findMovieById(id: Int): MovieDetail? {
        return findById(QUERY_MOVIE_BY_ID, id)
    }

    override suspend fun insertMovie(movie: MovieDetail) {
        insert(movie)
    }

    override suspend fun insertMovies(movies: List<MovieDetail>) {
        insert(movies)
    }

    override suspend fun findMovies(): List<MovieDetail> {
        return findAll(QUERY_MOVIE_DETAIL)
    }

    override suspend fun updateMovie(movie: MovieDetail) {
        update(movie)
    }

    override suspend fun deleteMovie(movie: MovieDetail) {
        delete(movie)
    }

    override suspend fun deleteMovies(movies: List<MovieDetail>) {
        delete(movies)
    }
}