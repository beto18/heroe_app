package com.santander.globile.superherotest.model.hero

data class Work(
    val base: String,
    val occupation: String
)