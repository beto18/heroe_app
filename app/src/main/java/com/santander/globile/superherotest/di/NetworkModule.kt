package com.santander.globile.superherotest.di

import android.app.Application
import android.util.Log
import com.santander.globile.superherotest.BuildConfig
import com.santander.globile.superherotest.domain.hero.ApiHero
import com.santander.globile.superherotest.domain.hero.ApiHeroHelper
import com.santander.globile.superherotest.domain.hero.impl.ApiHeroHelperImpl
import com.santander.globile.superherotest.domain.movie.ApiMovie
import com.santander.globile.superherotest.error.NetworkException
import com.santander.globile.superherotest.utils.ErrorUtils
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.ExperimentalCoroutinesApi
import okhttp3.*
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.IOException
import java.util.concurrent.TimeUnit
import javax.inject.Named
import javax.inject.Singleton

@ExperimentalCoroutinesApi
@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {


    @Singleton
    @Provides
    fun provideOkHttpCache(application: Application): Cache {
        val cacheSize = 10 * 1024 * 1024 // 10 MiB
        return Cache(application.cacheDir, cacheSize.toLong())
    }

    @Provides
    @Named("logging")
    fun provideLoggingInterceptors(): Interceptor {
        val interceptorLogger = HttpLoggingInterceptor()
        interceptorLogger.level = HttpLoggingInterceptor.Level.BODY
        return interceptorLogger
    }

    @Provides
    @Named("autho")
    fun provideAthorizationInterceptor(): Interceptor {
        return Interceptor {
            var request: Request = it.request()
            val url = request.url.newBuilder()
                .addQueryParameter("api_key", "57c03784e01c87db8bdafa013c6fdfe3")
                .build()
            Log.d("AUTHO", "URL: $url")
            request = request.newBuilder().url(url).build()
            Log.d("AUTHO", "URLRequest: ${request.url}")

            it.proceed(request)
        }
    }

    @Provides
    @Named("error")
    fun provideErrorInterceptor(): Interceptor {
        return Interceptor {
            try {
                val request: Request = it.request()
                val response: Response = it.proceed(request)
                if (response.code / 100 == 5) {
                    val error = ErrorUtils.getResourceError(request, response)
                    Log.w("Resources", "Error en el servidor: $error")
                }
                response
            } catch (e: IOException) {
                throw NetworkException("Ocurrió un problema de red", e)

            }
        }
    }

    @Provides
    @Singleton
    fun provideOkHttpClient(
        okHttpCache: Cache,
        @Named("logging") loggingInterceptor: Interceptor,
        @Named("error") errorInterceptor: Interceptor,
        @Named("autho") authoInterceptor: Interceptor
    ): OkHttpClient {
        val builder = OkHttpClient.Builder()
            .connectTimeout(BuildConfig.NETWORK_TIME_OUT, TimeUnit.SECONDS)
            .writeTimeout(BuildConfig.NETWORK_TIME_OUT, TimeUnit.SECONDS)
            .readTimeout(BuildConfig.NETWORK_TIME_OUT, TimeUnit.SECONDS)
            .cache(okHttpCache)
            .addInterceptor(loggingInterceptor)
            .addInterceptor(errorInterceptor)
            .addInterceptor(authoInterceptor)
        return builder.build()
    }

    @Provides
    @Singleton
    @Named("hero")
    fun provideRetrofitHero(
        okHttpClient: OkHttpClient
    ): Retrofit {
        return Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .client(okHttpClient)
            .baseUrl(BuildConfig.BASE_URL_HEROES)
            .build()
    }

    @Provides
    @Singleton
    @Named("movie")
    fun provideRetrofitMovie(
        okHttpClient: OkHttpClient
    ): Retrofit {
        return Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .client(okHttpClient)
            .baseUrl(BuildConfig.BASE_URL_MOVIES)
            .build()
    }

    @Provides
    @Singleton
    fun provideApiHeroService(@Named("hero") retrofit: Retrofit): ApiHero =
        retrofit.create(ApiHero::class.java)

    @Provides
    @Singleton
    fun provideApiMovieService(@Named("movie") retrofit: Retrofit): ApiMovie =
        retrofit.create(ApiMovie::class.java)

    @Provides
    @Singleton
    fun provideApiHeroHelper(apiHero: ApiHero): ApiHeroHelper =
        ApiHeroHelperImpl(apiHero)


}