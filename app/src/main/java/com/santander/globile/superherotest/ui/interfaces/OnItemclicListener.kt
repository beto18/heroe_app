package com.santander.globile.superherotest.ui.interfaces

interface OnItemclicListener {

    fun onItemClick(view: android.view.View, position: Int)
}