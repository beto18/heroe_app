package com.santander.globile.superherotest.ui.base

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.santander.globile.superherotest.utils.network.ConnectionProvider
import com.santander.globile.superherotest.utils.network.NetworkState
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import javax.inject.Inject

@ExperimentalCoroutinesApi
open class BaseViewModel : ViewModel() {

    @Inject
    lateinit var connectionProvider: ConnectionProvider

    companion object {
        val TAG: String = this::class.java.simpleName
    }


    val coroutineExceptionHandler = CoroutineExceptionHandler { _, exception ->
        println("Handle $exception in CoroutineExceptionHandler")
    }

    private val _stateNetwork by lazy { MutableStateFlow<NetworkState>(NetworkState.Loading) }
    val stateNetwork: StateFlow<NetworkState> = _stateNetwork.asStateFlow()

    fun initStateNetwork() {
        viewModelScope.launch(coroutineExceptionHandler) {
            connectionProvider.onState().catch {
                Log.e(TAG, "Ocurrio un error", it)
            }.collect { network ->
                if (network is NetworkState.Connected) {
                    _stateNetwork.value = network.copy(connect = true)
                } else {
                    _stateNetwork.value = network
                }
            }
        }
    }


}