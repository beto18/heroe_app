package com.santander.globile.superherotest.ui.adapter

import android.content.res.ColorStateList
import android.os.Build
import android.view.View
import androidx.appcompat.widget.AppCompatImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup
import com.santander.globile.superherotest.R
import com.santander.globile.superherotest.model.movie.Genre
import com.santander.globile.superherotest.utils.UiUtils


@BindingAdapter("imageUrl")
fun setImageGlide(container: AppCompatImageView, url: String?) {
    Glide.with(container.context)
        .load(url ?: R.drawable.not_found_image)
        .placeholder(R.drawable.image_loader)
        .thumbnail(0.05f)
        .centerCrop()
        .transition(DrawableTransitionOptions.withCrossFade())
        .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
        .error(R.drawable.not_found_image)
        .into(container)


}

@BindingAdapter("visibleGone")
fun showHide(view: View, show: Boolean) {
    if (show) view.visibility = View.VISIBLE else view.visibility = View.GONE
}

@BindingAdapter("items")
fun setItems(view: ChipGroup, genres: List<Genre?>?) {
    if (genres == null // Since we are using liveData to observe data, any changes in that table(favorites)
        // will trigger the observer and hence rebinding data, which can lead to duplicates.
        || view.childCount > 0
    ) return

    // dynamically create & add genre chips
    val context = view.context
    for (genre in genres) {
        val chip = Chip(context)
        chip.text = genre!!.name
        chip.chipStrokeWidth = UiUtils.dipToPixels(context, 1f)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            chip.chipStrokeColor = ColorStateList.valueOf(
                context.resources.getColor(R.color.md_blue_grey_200, null)
            )
        }
        chip.setChipBackgroundColorResource(android.R.color.transparent)
        view.addView(chip)
    }
}
