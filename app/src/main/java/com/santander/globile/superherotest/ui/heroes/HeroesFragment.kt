package com.santander.globile.superherotest.ui.heroes

import android.content.res.Configuration
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.recyclerview.widget.GridLayoutManager
import com.santander.globile.superherotest.databinding.FragmentHerosBinding
import com.santander.globile.superherotest.model.hero.Hero
import com.santander.globile.superherotest.ui.base.BaseFragment
import com.santander.globile.superherotest.ui.commons.UIState
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch


@AndroidEntryPoint
class HeroesFragment : BaseFragment<HeroesViewModel, FragmentHerosBinding>
    (FragmentHerosBinding::inflate) {
    override val vm: HeroesViewModel by viewModels()
    private var layoutManager: GridLayoutManager? = null
    override val TAG: String = HeroesFragment::class.java.name

    companion object {
        private const val SPAN_COUNT_DEFAULT_PORTRAIT = 4
        private const val SPAN_COUNT_DEFAULT_LANDSCAPE = 6

    }


    override fun setupUI() {
        super.setupUI()
        //binding.vm = vm
        initRecyclerview()

    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)

        // Checks the orientation of the screen
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            Toast.makeText(requireContext(), "landscape", Toast.LENGTH_SHORT).show();
        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            Toast.makeText(requireContext(), "portrait", Toast.LENGTH_SHORT).show();
        }
    }

    private fun initRecyclerview() {
        binding.recycler.apply {
            setHasFixedSize(true)
            setItemViewCacheSize(20)
        }
        val spanCount: Int
        if (getOrientation() == Configuration.ORIENTATION_PORTRAIT) {
            spanCount = SPAN_COUNT_DEFAULT_PORTRAIT
        } else {
            spanCount = SPAN_COUNT_DEFAULT_LANDSCAPE
        }
        binding.recycler.layoutManager = getLayoutManager(spanCount)


    }

    private fun getLayoutManager(spanCount: Int): GridLayoutManager {
        layoutManager = GridLayoutManager(requireContext(), spanCount)
        return layoutManager as GridLayoutManager
    }

    @Suppress("UNCHECKED_CAST")
    override fun setupVM() {
        super.setupVM()
        vm.getHeroes()
        lifecycleScope.launch {
            viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {
                vm.heroesFlow.collect {
                    if (it is UIState.Success<*>) {
                        val heroes = it.data!! as List<Hero>
                        vm.heroeAdapter.set(heroes)
                        vm.heroeAdapter.onItemClicked.apply {

                        }
                    } else if (it is UIState.Error) {
                        Toast.makeText(context, it.message, Toast.LENGTH_LONG).show()
                    }
                }
            }
        }

    }


}