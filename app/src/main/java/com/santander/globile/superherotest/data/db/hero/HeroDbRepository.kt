package com.santander.globile.superherotest.data.db.hero

import com.santander.globile.superherotest.model.hero.Hero

interface HeroDbRepository {

     suspend fun findHeroById(id: Int): Hero?

    suspend fun insertHero(hero: Hero)

    suspend fun insertHeroes(heroes: List<Hero>)

    suspend fun findHeroes(): List<Hero>

    suspend fun updateHeroes(hero: Hero)

    suspend fun deleteHero(hero: Hero)

    suspend fun deleteHeroes(heroes: List<Hero>)


}