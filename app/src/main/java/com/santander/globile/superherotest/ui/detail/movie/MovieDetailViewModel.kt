package com.santander.globile.superherotest.ui.detail.movie

import android.util.Log
import androidx.lifecycle.viewModelScope
import com.santander.globile.superherotest.error.ResourceNotFoundException
import com.santander.globile.superherotest.repository.MovieRepository
import com.santander.globile.superherotest.ui.base.BaseViewModel
import com.santander.globile.superherotest.ui.commons.UIState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@ExperimentalCoroutinesApi
@HiltViewModel
class MovieDetailViewModel @Inject constructor(
    private val movieRepository: MovieRepository,
) : BaseViewModel() {

    private val _movieFlow by lazy { MutableStateFlow<UIState>(UIState.Empty) }
    val heroFlow: StateFlow<UIState> = _movieFlow


    fun getHero(id: Int) {
        viewModelScope.launch {
            movieRepository.findMovieAndDetailById(id)
                .catch {
                    val error: String = if (it is ResourceNotFoundException) {
                        "No se encontraron datos"
                    } else {
                        it.message!!
                    }
                    UIState.Error(error)
                    Log.e(TAG, error, it)
                    _movieFlow.value = UIState.Error(error)
                }.collect {
                    _movieFlow.value = UIState.Success(it)
                }
        }
    }


}