package com.santander.globile.superherotest.repository.impl

import android.util.Log
import com.santander.globile.superherotest.data.dao.movie.MovieAndDetailDao
import com.santander.globile.superherotest.data.db.movie.MovieDbRepository
import com.santander.globile.superherotest.data.db.movie.MovieDetailDbRepository
import com.santander.globile.superherotest.domain.movie.ApiMovie
import com.santander.globile.superherotest.error.ResourceNotFoundException
import com.santander.globile.superherotest.model.movie.Movie
import com.santander.globile.superherotest.repository.MovieRepository
import com.santander.globile.superherotest.utils.ORIGINAL
import com.santander.globile.superherotest.utils.URL_IMAGE
import com.santander.globile.superherotest.utils.network.ConnectionProvider
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn

class MovieRepositoryImpl(
    private val api: ApiMovie,
    private val db: MovieDbRepository,
    private val connectionProvider: ConnectionProvider,
    private val repositoryMovieDetail: MovieDetailDbRepository,
    private val dao: MovieAndDetailDao
) : MovieRepository {

    companion object {
        private val TAG = MovieRepositoryImpl::class.java.name
    }

    override fun getMoviePopular() = flow {
        try {
            var movies: List<Movie> = db.findMovies()
            if (movies.isEmpty()) {
                connectionProvider.isConnected()?.let { connected ->
                    if (connected) {
                        movies = api.getMoviesPopular().results
                        movies.forEach { movie ->
                            movie.url = "$URL_IMAGE$ORIGINAL${movie.posterPath}"
                        }
                        db.insertMovies(movies)
                        emit(movies)
                    } else {
                        throw ResourceNotFoundException()
                    }
                }

            } else {
                emit(movies)
            }
        } catch (e: Exception) {
            Log.e(TAG, "Ocurrio un error: ", e)
            throw e
        }
    }.flowOn(Dispatchers.IO)

    override fun findMovieById(id: Int) = flow {
        emit(db.findMovieById(id)!!)
    }.flowOn(Dispatchers.IO)


    override suspend fun insertMovies(movies: List<Movie>) {
        db.insertMovies(movies)
    }

    override suspend fun insertMovie(movie: Movie) {
        db.insertMovie(movie)
    }

    override suspend fun deleteMovie(movie: Movie) {
        db.deleteMovie(movie)
    }

    override suspend fun deleteMovies(movies: List<Movie>) {
        db.deleteMovies(movies)
    }

    override fun findMovieAndDetailById(id: Int) = flow {
        var movieDetail = dao.findById(id)
        if (movieDetail == null) {
            connectionProvider.isConnected()?.let { connected ->
                if (connected) {
                    val moviDetail = api.getMovieDetail(id)
                    repositoryMovieDetail.insertMovie(moviDetail)
                    movieDetail = dao.findById(id)
                    emit(movieDetail!!)
                } else {
                    throw ResourceNotFoundException()
                }
            }

        } else {
            emit(movieDetail!!)
        }
    }.flowOn(Dispatchers.IO)


}