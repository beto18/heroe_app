package com.santander.globile.superherotest.ui.viewholder

import com.santander.globile.superherotest.databinding.ItemHeroBinding
import com.santander.globile.superherotest.model.hero.Hero
import com.santander.globile.superherotest.ui.base.BaseViewHolder

class HeroViewHolder(
    private val binding: ItemHeroBinding,
    override val onClick: (data: Int) -> Unit
) : BaseViewHolder<Hero>(binding) {

    override fun bindTo(data: Hero) {
        binding.data = data
        binding.executePendingBindings()
    }


}