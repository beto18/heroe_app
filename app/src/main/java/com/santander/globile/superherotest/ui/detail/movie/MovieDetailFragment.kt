package com.santander.globile.superherotest.ui.detail.movie

import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import com.santander.globile.superherotest.collectLastestLyfeCycleFlow
import com.santander.globile.superherotest.databinding.MovieDetailFragmentBinding
import com.santander.globile.superherotest.model.movie.MovieAndDetail
import com.santander.globile.superherotest.ui.base.BaseFragment
import com.santander.globile.superherotest.ui.commons.UIState
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalCoroutinesApi
@AndroidEntryPoint
class MovieDetailFragment :
    BaseFragment<MovieDetailViewModel, MovieDetailFragmentBinding>(MovieDetailFragmentBinding::inflate) {

    override val vm: MovieDetailViewModel by viewModels()
    private val args by navArgs<MovieDetailFragmentArgs>()
    override val TAG: String = MovieDetailFragment::class.java.name

    override fun setupUI() {
        super.setupUI()
        binding.vm = vm
        vm.getHero(args.id)

    }

    override fun setupVM() {
        super.setupVM()
        requireActivity().collectLastestLyfeCycleFlow(vm.heroFlow) { uiState ->
            when (uiState) {
                is UIState.Success<*> -> {
                    binding.movieAndDetail = uiState.data as MovieAndDetail
                }
                is UIState.Error -> {
                    Toast.makeText(requireContext(), uiState.message, Toast.LENGTH_LONG)
                        .show()
                }
                else -> {

                }
            }


        }


    }

}