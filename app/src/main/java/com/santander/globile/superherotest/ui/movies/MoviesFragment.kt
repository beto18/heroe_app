package com.santander.globile.superherotest.ui.movies

import android.content.res.Configuration
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.santander.globile.superherotest.collectLastestLyfeCycleFlow
import com.santander.globile.superherotest.databinding.FragmentHerosBinding
import com.santander.globile.superherotest.model.movie.Movie
import com.santander.globile.superherotest.showToast
import com.santander.globile.superherotest.ui.base.BaseFragment
import com.santander.globile.superherotest.ui.commons.SpaceItemDecoration
import com.santander.globile.superherotest.ui.commons.UIState
import com.santander.globile.superherotest.utils.network.NetworkState
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi


@ExperimentalCoroutinesApi
@AndroidEntryPoint
class MoviesFragment : BaseFragment<MoviesViewModel, FragmentHerosBinding>
    (FragmentHerosBinding::inflate) {
    override val vm: MoviesViewModel by viewModels()
    private var layoutManager: GridLayoutManager? = null
    override val TAG: String = MoviesFragment::class.java.name
    private val itemClick = { movie: Movie ->
        val id = movie.id
        val action =
            MoviesFragmentDirections.actionMoviesFragmentToMovieDetailFragment(id.toInt())
        findNavController().navigate(action)

    }

    companion object {
        private const val SPAN_COUNT_DEFAULT_PORTRAIT = 3
        private const val SPAN_COUNT_DEFAULT_LANDSCAPE = 5

    }


    override fun setupUI() {
        super.setupUI()
        binding.vm = vm
        if (getOrientation() == Configuration.ORIENTATION_PORTRAIT) {
            initRecyclerview(SPAN_COUNT_DEFAULT_PORTRAIT)
        } else {
            initRecyclerview(SPAN_COUNT_DEFAULT_LANDSCAPE)
        }


    }


    private fun initRecyclerview(spanCount: Int) {
        binding.recycler.apply {
            setHasFixedSize(true)
            setItemViewCacheSize(20)
            addItemDecoration(SpaceItemDecoration(32, spanCount))
        }
        binding.recycler.layoutManager = getLayoutManager(spanCount)
    }

    private fun getLayoutManager(spanCount: Int): GridLayoutManager {
        layoutManager = GridLayoutManager(requireContext(), spanCount)
        return layoutManager as GridLayoutManager
    }

    override fun setupVM() {
        super.setupVM()
        vm.initStateNetwork()
        vm.getMovies()
        requireActivity().collectLastestLyfeCycleFlow(vm.stateNetwork) { network ->
            var message: String? = null
            when (network) {
                is NetworkState.Connected -> vm.getMovies()
                is NetworkState.NoConnected -> message = "No cuenta con conexión a internet"
                else -> {}
            }
            message?.let { requireActivity().showToast(it) }


        }
        requireActivity().collectLastestLyfeCycleFlow(vm.moviesFlow) { uiState ->
            when (uiState) {
                is UIState.Success<*> -> {
                    @Suppress("UNCHECKED_CAST")
                    val movies = (uiState.data as List<Movie>)
                    vm.movieAdapter.set(movies)
                    vm.movieAdapter.onItemClicked = itemClick
                }
                is UIState.Error -> {
                    uiState.message?.let { message ->
                        requireActivity().showToast(message)
                    }
                }
                else -> {}
            }

        }


    }


}