package com.santander.globile.superherotest.model.hero

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = Hero.TABLE_NAME)
data class Hero(
    @PrimaryKey
    val id: String,
    val appearance: Appearance? = null,
    val biography: Biography? = null,
    val connections: Connections? = null,
    val image: Image? = null,
    val name: String? = null,
    val powerstats: Powerstats? = null,
    val response: String? = null,
    val work: Work? = null
) {

    companion object {
         const val TABLE_NAME = "heroe"
    }


}
