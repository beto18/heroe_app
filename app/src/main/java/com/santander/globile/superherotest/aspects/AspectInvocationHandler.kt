package com.santander.globile.superherotest.aspects

import android.util.Log
import java.lang.reflect.InvocationHandler
import java.lang.reflect.Method

class AspectInvocationHandler(
    val type: Class<*>,
    val instance: Any? = null,
    var interceptors: MutableMap<Method, List<Interceptor<*>>>? = mutableMapOf()
) : InvocationHandler {


    init {

        val methods = type.methods
        for (method in methods) {
            val interceptors = getInterceptors(method)
            interceptors?.let {
                this.interceptors!!.put(method, interceptors)
            }
        }
    }


    override fun invoke(p0: Any?, method: Method?, args: Array<out Any>?): Any {
        val interceptors = this.interceptors!![method!!]
        interceptors?.let {
            for (interceptor in it) {
                try {
                    Log.v(
                        TAG, java.lang.String.format(
                            "Ejecutando interceptor %s BEFORE %s.%s",
                            interceptor.name, method.declaringClass.name, method.name
                        )
                    )
                    interceptor.before(instance, method, args)

                } catch (e: Exception) {
                    Log.e(
                        TAG, java.lang.String.format(
                            "Error interceptor %s BEFORE %s.%s",
                            interceptor.name, method.declaringClass.name, method.name
                        ), e
                    )
                }
            }
        }

        var returnValue = method.invoke(instance, *(args ?: arrayOfNulls<Any>(0)))
        interceptors?.let {
            for (interceptor in it) {
                try {
                    Log.v(
                        TAG, java.lang.String.format(
                            "Ejecutando interceptor %s AFTER %s.%s",
                            interceptor.name, method.declaringClass.name, method.name
                        )
                    )
                    returnValue = interceptor.after(instance, method, returnValue)
                } catch (e: java.lang.Exception) {
                    Log.e(
                        TAG, java.lang.String.format(
                            "Error interceptor %s AFTER %s.%s",
                            interceptor.name, method.declaringClass.name, method.name
                        ), e
                    )
                }
            }

        }
        return returnValue!!
    }

    companion object {

        private val TAG = AspectInvocationHandler::class.java.simpleName

        private fun getInterceptors(method: Method): List<Interceptor<*>>? {
            val interceptors = arrayListOf<Interceptor<*>>()
            val annotations = method.annotations
            for (annotation in annotations) {
                try {
                    val aspect = annotation.annotationClass.java.getAnnotation(Aspect::class.java)
                    aspect?.let {
                        val interceptor = aspect.interceptor.java.newInstance()
                        interceptor.setAnnotation(annotation)
                        interceptors.add(interceptor)
                    }
                } catch (e: Exception) {
                    Log.e(
                        TAG, String.format(
                            "Error al obtener los interceptores de método %s.%s",
                            method.declaringClass.name, method.name
                        ), e
                    )
                }
            }

            return if (interceptors.isEmpty()) null else interceptors
        }
    }
}