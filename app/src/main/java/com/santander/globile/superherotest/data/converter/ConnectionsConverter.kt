package com.santander.globile.superherotest.data.converter

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.santander.globile.superherotest.model.hero.Connections

class ConnectionsConverter {

    @TypeConverter
    fun connectionsToString(connections: Connections): String = Gson().toJson(connections)

    @TypeConverter
    fun stringToConnections(string: String): Connections =
        Gson().fromJson(string, Connections::class.java)
}