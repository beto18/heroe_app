package com.santander.globile.superherotest.data.db.impl

import com.santander.globile.superherotest.data.db.hero.HeroDbRepository
import com.santander.globile.superherotest.data.room.HeroAppDb
import com.santander.globile.superherotest.data.room.RoomRepository
import com.santander.globile.superherotest.model.hero.Hero
import javax.inject.Inject

class HeroDbRepositoryImpl @Inject constructor(db: HeroAppDb) :
    RoomRepository<Hero>(db.heroDao), HeroDbRepository {

    companion object {

        private const val QUERY_HEROES = "SELECT * FROM heroe"

        private const val QUERY_HERO_BY_ID = "SELECT * FROM heroe WHERE id = ?"
    }

    override suspend fun insertHeroes(heroes: List<Hero>) {
        super.insert(heroes)
    }

    override suspend fun findHeroes(): List<Hero> {
        return findAll(QUERY_HEROES)
    }

    override suspend fun findHeroById(id: Int): Hero {
        return findById(QUERY_HERO_BY_ID, id)
    }

    override suspend fun insertHero(hero: Hero) {
        super.insert(hero)
    }

    override suspend fun updateHeroes(hero: Hero) {
        super.update(hero)
    }

    override suspend fun deleteHero(hero: Hero) {
        super.delete(hero)
    }

    override suspend fun deleteHeroes(heroes: List<Hero>) {
        super.delete(heroes)
    }


}