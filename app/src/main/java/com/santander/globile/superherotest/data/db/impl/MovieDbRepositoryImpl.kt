package com.santander.globile.superherotest.data.db.impl

import com.santander.globile.superherotest.data.db.movie.MovieDbRepository
import com.santander.globile.superherotest.data.room.HeroAppDb
import com.santander.globile.superherotest.data.room.RoomRepository
import com.santander.globile.superherotest.model.movie.Movie
import javax.inject.Inject

class MovieDbRepositoryImpl @Inject constructor(db: HeroAppDb) :
    RoomRepository<Movie>(db.movieDao),
    MovieDbRepository {

    companion object {

        private const val QUERY_MOVIE = "SELECT * FROM movie"

        private const val QUERY_MOVIE_BY_ID = "SELECT * FROM movie WHERE id = ?"
    }

    override fun findMovieById(id: Int): Movie? {
        return findById(QUERY_MOVIE_BY_ID, id)
    }

    override fun insertMovie(movie: Movie) {
        insertMovie(movie)
    }

    override fun insertMovies(movies: List<Movie>) {
        insert(movies)
    }

    override fun findMovies(): List<Movie> {
        return findAll(QUERY_MOVIE)
    }

    override fun updateMovie(movie: Movie) {
        update(movie)
    }

    override fun deleteMovie(movie: Movie) {
        delete(movie)
    }

    override fun deleteMovies(movies: List<Movie>) {
        delete(movies)
    }


}