package com.santander.globile.superherotest.ui.viewholder

import com.santander.globile.superherotest.databinding.ItemMovieBinding
import com.santander.globile.superherotest.model.movie.Movie
import com.santander.globile.superherotest.ui.base.BaseViewHolder

class MovieViewHolder(
    private val binding: ItemMovieBinding,
    override val onClick: (data: Int) -> Unit
) : BaseViewHolder<Movie>(binding) {

    override fun bindTo(data: Movie) {
        binding.data = data
        binding.executePendingBindings()
    }


}