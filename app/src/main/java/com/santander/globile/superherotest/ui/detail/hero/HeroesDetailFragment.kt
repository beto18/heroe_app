package com.santander.globile.superherotest.ui.detail.hero

import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.navArgs
import com.santander.globile.superherotest.databinding.DetailHeroFragmentBinding
import com.santander.globile.superherotest.model.hero.Hero
import com.santander.globile.superherotest.ui.base.BaseFragment
import com.santander.globile.superherotest.ui.commons.UIState
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

@AndroidEntryPoint
class HeroesDetailFragment :
    BaseFragment<HeroesDetailViewModel, DetailHeroFragmentBinding>(DetailHeroFragmentBinding::inflate) {

    override val vm: HeroesDetailViewModel by viewModels()
    private val args by navArgs<HeroesDetailFragmentArgs>()
    override val TAG: String = HeroesDetailFragment::class.java.name

    override fun setupUI() {
        super.setupUI()
        binding.vm = vm
    }

    override fun setupVM() {
        super.setupVM()
        vm.getHero(args.id)
        lifecycleScope.launch {
            viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {
                vm.hero.collect { uiState ->
                    if (uiState is UIState.Success<*>) {
                        binding.heroe = uiState.data as Hero
                    } else if (uiState is UIState.Error) {
                        Toast.makeText(requireContext(), uiState.message, Toast.LENGTH_LONG)
                            .show()

                    }

                }
            }
        }

    }

}