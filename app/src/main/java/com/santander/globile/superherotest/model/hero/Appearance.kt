package com.santander.globile.superherotest.model.hero

data class Appearance(
    val gender: String,
    val race: String,
)