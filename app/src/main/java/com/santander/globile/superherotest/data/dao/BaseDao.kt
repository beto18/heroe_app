package com.santander.globile.superherotest.data.dao

import androidx.room.*
import androidx.sqlite.db.SupportSQLiteQuery


interface BaseDao<T> {

    @RawQuery
    fun findById(query: SupportSQLiteQuery): T

    @RawQuery
    fun findAll(query: SupportSQLiteQuery): List<T>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(entity: T)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(entities: List<T>)

    @Update
    fun update(entity: T)

    @Update
    fun updateAll(entities: List<T>)

    @Delete
    fun delete(entity: T)

    @Delete
    fun delete(entities: List<T>)

}