package com.santander.globile.superherotest.aspects

import kotlin.reflect.KClass


/**
 * Anotación para indicar el modo de uso del aspecto.
 */
@MustBeDocumented
@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.ANNOTATION_CLASS)
annotation class Aspect(val interceptor: KClass<out Interceptor<out Annotation>>)

