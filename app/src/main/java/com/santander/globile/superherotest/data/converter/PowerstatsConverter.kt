package com.santander.globile.superherotest.data.converter

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.santander.globile.superherotest.model.hero.Powerstats

class PowerstatsConverter {

    @TypeConverter
    fun powerstatsToString(powerstats: Powerstats): String = Gson().toJson(powerstats)

    @TypeConverter
    fun stringToPowerstats(string: String): Powerstats =
        Gson().fromJson(string, Powerstats::class.java)
}