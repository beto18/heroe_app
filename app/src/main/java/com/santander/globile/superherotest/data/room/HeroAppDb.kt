package com.santander.globile.superherotest.data.room

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.santander.globile.superherotest.data.converter.*
import com.santander.globile.superherotest.data.dao.hero.HeroDao
import com.santander.globile.superherotest.data.dao.movie.MovieAndDetailDao
import com.santander.globile.superherotest.data.dao.movie.MovieDao
import com.santander.globile.superherotest.data.dao.movie.MovieDetailDao
import com.santander.globile.superherotest.model.hero.Hero
import com.santander.globile.superherotest.model.movie.Movie
import com.santander.globile.superherotest.model.movie.MovieDetail

@Database(
    entities =
    [Hero::class,
        Movie::class,
        MovieDetail::class], version = 1, exportSchema = false
)
@TypeConverters(
    AppearanceConverter::class,
    BiographyConverter::class,
    ConnectionsConverter::class,
    ImageConverter::class,
    PowerstatsConverter::class,
    WorkConverter::class,
    GenresConverter::class
)
abstract class HeroAppDb : RoomDatabase() {
    abstract val heroDao: HeroDao
    abstract val movieDao: MovieDao
    abstract val movieDetailDao: MovieDetailDao
    abstract val movieAndDetailDao: MovieAndDetailDao


}

