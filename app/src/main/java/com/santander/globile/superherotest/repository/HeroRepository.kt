package com.santander.globile.superherotest.repository

import com.santander.globile.superherotest.model.hero.Hero
import kotlinx.coroutines.flow.Flow

interface HeroRepository {

    fun getHeroes(): Flow<List<Hero>>

    fun findHeroById(id: Int): Flow<Hero>

    suspend fun insertHeroes(heroes: List<Hero>)

    suspend fun insertHero(hero: Hero)

    suspend fun deleteHero(hero: Hero)

    suspend fun deleteHeroes(heroes: List<Hero>)
}