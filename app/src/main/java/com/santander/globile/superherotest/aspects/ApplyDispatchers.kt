package com.santander.globile.superherotest.aspects


/**
 * Aplica el Dispatcher.IO en <code>flowOn</code>
 * de un {@link Flow<*> }
 */
@MustBeDocumented
@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.FIELD, AnnotationTarget.FUNCTION, AnnotationTarget.CLASS)
@Aspect(interceptor = ApplyDispatchersInterceptor::class)
annotation class ApplyDispatchers(val enabled: Boolean = true)