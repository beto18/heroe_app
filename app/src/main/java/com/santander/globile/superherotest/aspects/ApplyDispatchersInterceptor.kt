package com.santander.globile.superherotest.aspects

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOn
import java.lang.reflect.Method

/**
 * Interceptor para aplicar  un Dispacher.IO
 * al suscribirse a un {@link Flow<*>}
 */
class ApplyDispatchersInterceptor : Interceptor<ApplyDispatchers>() {


    override fun after(
        target: Any?,
        method: Method?,
        returnValue: Any?,
        annotation: ApplyDispatchers?
    ): Any {
        if (annotation!!.enabled && returnValue is Flow<*>) {
            return returnValue.flowOn(Dispatchers.IO)
        }
        return returnValue!!
    }
}