package com.santander.globile.superherotest.data.dao.hero

import androidx.room.Dao
import com.santander.globile.superherotest.data.dao.BaseDao
import com.santander.globile.superherotest.model.hero.Hero

@Dao
interface HeroDao : BaseDao<Hero>

@Dao
interface VillainDao : BaseDao<Hero>