package com.santander.globile.superherotest.model.hero

data class Image(
    val url: String
)