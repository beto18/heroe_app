package com.santander.globile.superherotest.di

import com.santander.globile.superherotest.data.db.hero.HeroDbRepository
import com.santander.globile.superherotest.data.db.impl.HeroDbRepositoryImpl
import com.santander.globile.superherotest.data.db.impl.MovieDbRepositoryImpl
import com.santander.globile.superherotest.data.db.impl.MovieDetailDbRepositoryImpl
import com.santander.globile.superherotest.data.db.movie.MovieDbRepository
import com.santander.globile.superherotest.data.db.movie.MovieDetailDbRepository
import com.santander.globile.superherotest.data.room.HeroAppDb
import com.santander.globile.superherotest.domain.hero.ApiHeroHelper
import com.santander.globile.superherotest.domain.movie.ApiMovie
import com.santander.globile.superherotest.repository.HeroRepository
import com.santander.globile.superherotest.repository.MovieRepository
import com.santander.globile.superherotest.repository.impl.HeroRepositoryImpl
import com.santander.globile.superherotest.repository.impl.MovieRepositoryImpl
import com.santander.globile.superherotest.utils.network.ConnectionProvider
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent


@Module
@InstallIn(ViewModelComponent::class)
object RepositoryModule {


    @Provides
    fun provideRepositoryHero(db: HeroAppDb): HeroDbRepository {
        return HeroDbRepositoryImpl(db)
    }


    @Provides
    fun provideRepositoryMovieDetail(db: HeroAppDb): MovieDetailDbRepository {
        return MovieDetailDbRepositoryImpl(db)
    }

    @Provides
    fun provideRepositoryMovie(db: HeroAppDb): MovieDbRepository {
        return MovieDbRepositoryImpl(db)
    }


    @Provides
    fun provideInteractorHero(
        apiHelper: ApiHeroHelper,
        repository: HeroDbRepository,
        connectionProvider: ConnectionProvider
    ): HeroRepository {
        return HeroRepositoryImpl(apiHelper, repository, connectionProvider)
    }

    @Provides
    fun provideInteractorMovie(
        api: ApiMovie,
        repository: MovieDbRepository,
        connectionProvider: ConnectionProvider,
        repositoryMD: MovieDetailDbRepository,
        appDb: HeroAppDb
    ): MovieRepository {
        return MovieRepositoryImpl(
            api,
            repository,
            connectionProvider,
            repositoryMD,
            appDb.movieAndDetailDao
        )
    }
}

