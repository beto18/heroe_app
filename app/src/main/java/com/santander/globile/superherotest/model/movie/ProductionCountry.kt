package com.santander.globile.superherotest.model.movie

data class ProductionCountry(
    val iso_3166_1: String,
    val name: String
)