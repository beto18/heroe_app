package com.santander.globile.superherotest.data.converter

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.santander.globile.superherotest.model.hero.Appearance

class AppearanceConverter {
    @TypeConverter
    fun appearanceToString(app: Appearance): String = Gson().toJson(app)

    @TypeConverter
    fun stringToAppearance(string: String): Appearance =
        Gson().fromJson(string, Appearance::class.java)

}