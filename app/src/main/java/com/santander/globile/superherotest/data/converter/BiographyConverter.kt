package com.santander.globile.superherotest.data.converter

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.santander.globile.superherotest.model.hero.Biography


class BiographyConverter {
    @TypeConverter
    fun biographyToString(biography: Biography): String = Gson().toJson(biography)

    @TypeConverter
    fun stringToBiography(string: String): Biography =
        Gson().fromJson(string, Biography::class.java)

}