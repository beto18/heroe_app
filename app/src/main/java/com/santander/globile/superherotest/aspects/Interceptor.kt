package com.santander.globile.superherotest.aspects

import java.lang.reflect.Method

/**
 * Interceptor de aspecto.
 */
abstract class Interceptor<AspectAnnotation : Annotation?> {
    /**
     * Anotación.
     */

    private var annotation: AspectAnnotation? = null
    fun setAnnotation(annotation: Annotation) {
        this.annotation = annotation as AspectAnnotation
    }

    val name: String
        get() = annotation.toString()

    /**
     * Ejecución antes del método aspectado.
     */
    fun before(target: Any?, method: Method?, args: Array<out Any>?) {
        before(target, method, args, annotation)
    }

    /**
     * Ejecución despúes del método aspectado.
     * @param returnValue Valor de retorno del método aspectado.
     * @return Valor de retorno.
     */
    fun after(target: Any?, method: Method?, returnValue: Any?): Any? {
        return after(target, method, returnValue, annotation)
    }

    /**
     * Ejecución antes del método aspectado.
     * @param annotation Anotación.
     */
    protected fun before(
        target: Any?,
        method: Method?,
        args: Array<out Any>?,
        annotation: AspectAnnotation?
    ) {
    }

    /**
     * Ejecución despúes del retorno de valor.
     * @param annotation Anotación.
     * @param returnValue Valor de retorno del método aspectado.
     * @return Valor de retorno.
     */
    protected open fun after(
        target: Any?,
        method: Method?,
        returnValue: Any?,
        annotation: AspectAnnotation?
    ): Any? {
        return returnValue
    }
}