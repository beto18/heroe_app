package com.santander.globile.superherotest.ui.movies

import androidx.lifecycle.viewModelScope
import com.santander.globile.superherotest.error.ResourceNotFoundException
import com.santander.globile.superherotest.repository.MovieRepository
import com.santander.globile.superherotest.ui.adapter.MovieAdapter
import com.santander.globile.superherotest.ui.base.BaseViewModel
import com.santander.globile.superherotest.ui.commons.UIState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import javax.inject.Inject

@ExperimentalCoroutinesApi
@HiltViewModel
class MoviesViewModel @Inject constructor(
    private val interactor: MovieRepository
) : BaseViewModel() {


    val movieAdapter by lazy { MovieAdapter() }

    private val _moviesFlow = MutableStateFlow<UIState>(UIState.Empty)
    val moviesFlow: StateFlow<UIState> = _moviesFlow.asStateFlow()

    fun getMovies() {
        viewModelScope.launch(coroutineExceptionHandler) {
            interactor.getMoviePopular().catch {
                if (it !is ResourceNotFoundException) {
                    _moviesFlow.value = UIState.Error(it.message)
                }
            }.collectLatest {
                _moviesFlow.value = UIState.Success(it)

            }
        }
    }


}