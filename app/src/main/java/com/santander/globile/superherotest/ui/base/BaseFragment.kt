package com.santander.globile.superherotest.ui.base

import android.content.res.Configuration
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.viewbinding.ViewBinding
import com.santander.globile.superherotest.ui.interfaces.ConnectableFragment

typealias Inflate<VB> = (LayoutInflater, ViewGroup?, Boolean) -> VB

abstract class BaseFragment<T : ViewModel, VB : ViewBinding>(
    private val inflate: Inflate<VB>
) : Fragment(), ConnectableFragment {
    abstract val vm: T
    private var _binding: VB? = null
    abstract val TAG: String
    val binding get() = _binding!!


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = inflate.invoke(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupUI()
        setupVM()
    }


    protected open fun setupUI() = Unit

    protected open fun setupVM() = Unit

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    protected fun getOrientation(): Int {
        return activity?.resources?.configuration?.orientation ?: Configuration.ORIENTATION_PORTRAIT
    }

    override fun onConnected() {
        Log.d(TAG, "Conectado a internet")
        val fragments = childFragmentManager.fragments
        for (fragment in fragments) {
            if (fragment is ConnectableFragment) {
                fragment.onConnected()
            }
        }

    }

    override fun onDisconnect() {
        Log.d(TAG, "Desconectado de internet")
        val fragments = childFragmentManager.fragments
        for (fragment in fragments) {
            if (fragment is ConnectableFragment) {
                fragment.onDisconnect()
            }
        }
    }


}