package com.santander.globile.superherotest.model.movie

data class Genre(
    val id: Int,
    val name: String
)