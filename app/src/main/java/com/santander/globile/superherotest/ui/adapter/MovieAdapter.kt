package com.santander.globile.superherotest.ui.adapter

import com.santander.globile.superherotest.databinding.ItemMovieBinding
import com.santander.globile.superherotest.model.movie.Movie
import com.santander.globile.superherotest.ui.base.BaseAdapter
import com.santander.globile.superherotest.ui.base.BaseViewHolder
import com.santander.globile.superherotest.ui.viewholder.MovieViewHolder

class MovieAdapter : BaseAdapter<Movie, ItemMovieBinding>(ItemMovieBinding::inflate) {

    override var onItemClicked: (data: Movie) -> Unit = {}

    override fun set(data: List<Movie>) {
        submitList(data)
    }

    override fun viewHolder(binding: ItemMovieBinding): BaseViewHolder<Movie> {
        return MovieViewHolder(binding) { onItemClicked(getItem(it)) }
    }
}