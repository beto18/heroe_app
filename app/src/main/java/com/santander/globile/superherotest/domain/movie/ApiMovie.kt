package com.santander.globile.superherotest.domain.movie

import com.santander.globile.superherotest.model.movie.MovieDetail
import com.santander.globile.superherotest.model.movie.MovieResponse
import retrofit2.http.GET
import retrofit2.http.Path

interface ApiMovie {

    @GET("movie/popular")
    suspend fun getMoviesPopular(): MovieResponse

    @GET("movie/{movie_id}")
    suspend fun getMovieDetail(@Path("movie_id") id: Int): MovieDetail


}