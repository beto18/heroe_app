package com.santander.globile.superherotest.ui.interfaces

interface ConnectableFragment {

    fun onConnected()
    fun onDisconnect()
}